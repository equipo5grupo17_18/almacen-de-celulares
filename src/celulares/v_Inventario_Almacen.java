package celulares;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

public class v_Inventario_Almacen {
    public v_Inventario_Almacen(){
    marco();
    }
    
    public void marco(){
        JFrame f = new JFrame();
        f.setSize(400,200);
        f.setLocation(200,100);
        f.setTitle("v_i_a");
                
        JPanel panelmayor = new JPanel(new BorderLayout());
        
        JPanel panelnorte  = new JPanel();
        JPanel panelcentro = new JPanel();
        JPanel panelsur    = new JPanel();
        JPanel paneleste   = new JPanel();
        JPanel paneloeste  = new JPanel();
        
        FlowLayout fll1 = new FlowLayout(FlowLayout.CENTER); //Layout Manager
        panelnorte.setLayout(fll1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelcentro.setLayout(gbl1);
        
        FlowLayout fll2 = new FlowLayout();
        panelsur.setLayout(fll2);
        
        GridLayout gl2 = new GridLayout(1,0);
        paneleste.setLayout(gl2);
        
        GridLayout gl3 = new GridLayout(1,0);
        paneloeste.setLayout(gl3);
        
        //panel norte  
        JButton bn1 = new JButton("Agregar");
        JButton bn2 = new JButton("Modificar");
        JButton bn3 = new JButton("Borrar");
                
        //panel oeste
        JTable tabla = new JTable();

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Marca", "Modelo", "Precio", "Piezas"
            }
        ));

        //panel sur
        JButton bs1 = new JButton("Guardar");
        JButton bs2 = new JButton("Cancelar");
                   
        //panel centro
        
        //se agregan cosas a los paneles
        panelnorte.add(bn1);
        panelnorte.add(bn2);
        panelnorte.add(bn3);

        gbc1.gridx = 0;
        gbc1.gridy = 0;
        //gbc1.weithy = 1.0;
        gbc1.anchor = GridBagConstraints.WEST;
        
        panelcentro.add(tabla);
        
        panelsur.add(bs1);
        panelsur.add(bs2);
                
        panelmayor.add(panelnorte, BorderLayout.NORTH);
        panelmayor.add(panelcentro, BorderLayout.CENTER);
        panelmayor.add(paneleste, BorderLayout.EAST);
        panelmayor.add(panelsur, BorderLayout.SOUTH);
        panelmayor.add(paneloeste, BorderLayout.WEST);
        
        f.add(panelmayor);
        
        f.setVisible(true);
    }

    void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
