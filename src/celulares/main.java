/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celulares;

/**
 *
 * @author Nixerck
 */
public class main extends javax.swing.JFrame {

    /**
     * Creates new form main
     */
    public main() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menubar = new javax.swing.JMenuBar();
        producto = new javax.swing.JMenu();
        pcompras = new javax.swing.JMenuItem();
        pventas = new javax.swing.JMenuItem();
        inventario = new javax.swing.JMenu();
        ialmacen = new javax.swing.JMenuItem();
        iapartado = new javax.swing.JMenuItem();
        reportes = new javax.swing.JMenu();
        rcompras = new javax.swing.JMenuItem();
        rventas = new javax.swing.JMenuItem();
        ralmacen = new javax.swing.JMenuItem();
        rapartado = new javax.swing.JMenuItem();
        salir = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inventario");
        setLocation(new java.awt.Point(90, 90));

        producto.setText("Producto");

        pcompras.setText("Compras");
        pcompras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pcomprasActionPerformed(evt);
            }
        });
        producto.add(pcompras);

        pventas.setText("Ventas");
        pventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pventasActionPerformed(evt);
            }
        });
        producto.add(pventas);

        menubar.add(producto);

        inventario.setText("Inventario");

        ialmacen.setText("Almacen");
        ialmacen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ialmacenActionPerformed(evt);
            }
        });
        inventario.add(ialmacen);

        iapartado.setText("Apartado");
        iapartado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                iapartadoActionPerformed(evt);
            }
        });
        inventario.add(iapartado);

        menubar.add(inventario);

        reportes.setText("Reportes");

        rcompras.setText("Compras");
        rcompras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rcomprasActionPerformed(evt);
            }
        });
        reportes.add(rcompras);

        rventas.setText("Ventas");
        reportes.add(rventas);

        ralmacen.setText("Almacen");
        reportes.add(ralmacen);

        rapartado.setText("Apartado");
        reportes.add(rapartado);

        menubar.add(reportes);

        salir.setText("Salir");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        menubar.add(salir);

        setJMenuBar(menubar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 286, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pcomprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pcomprasActionPerformed
        new v_Producto_Compras().setVisible(true);
    }//GEN-LAST:event_pcomprasActionPerformed

    private void pventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pventasActionPerformed
        new v_Producto_Ventas().setVisible(true);
    }//GEN-LAST:event_pventasActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salirActionPerformed

    private void ialmacenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ialmacenActionPerformed
        new v_Inventario_Almacen().setVisible(true);
    }//GEN-LAST:event_ialmacenActionPerformed

    private void iapartadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_iapartadoActionPerformed
        new v_Inventario_Apartado().setVisible(true);
    }//GEN-LAST:event_iapartadoActionPerformed

    private void rcomprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rcomprasActionPerformed
        new v_Reportes_Compras().setVisible(true);
    }//GEN-LAST:event_rcomprasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ialmacen;
    private javax.swing.JMenuItem iapartado;
    private javax.swing.JMenu inventario;
    private javax.swing.JMenuBar menubar;
    private javax.swing.JMenuItem pcompras;
    private javax.swing.JMenu producto;
    private javax.swing.JMenuItem pventas;
    private javax.swing.JMenuItem ralmacen;
    private javax.swing.JMenuItem rapartado;
    private javax.swing.JMenuItem rcompras;
    private javax.swing.JMenu reportes;
    private javax.swing.JMenuItem rventas;
    private javax.swing.JMenu salir;
    // End of variables declaration//GEN-END:variables
}
