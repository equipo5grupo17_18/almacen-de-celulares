package celulares;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class v_Inventario_Apartado {
    public v_Inventario_Apartado(){
    marco();
    }
    
    public void marco(){
        JFrame f = new JFrame();
        f.setSize(300,300);
        f.setLocation(200,100);
        f.setTitle("v_i_a");
                
        JPanel panelmayor = new JPanel(new BorderLayout());
        
        JPanel panelnorte  = new JPanel();
        JPanel panelcentro = new JPanel();
        JPanel panelsur    = new JPanel();
        JPanel paneleste   = new JPanel();
        JPanel paneloeste  = new JPanel();
        
        FlowLayout fll1 = new FlowLayout(FlowLayout.CENTER); //Layout Manager
        panelnorte.setLayout(fll1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelcentro.setLayout(gbl1);
        
        FlowLayout fll2 = new FlowLayout();
        panelsur.setLayout(fll2);
        
        GridLayout gl2 = new GridLayout(1,0);
        paneleste.setLayout(gl2);
        
        GridLayout gl3 = new GridLayout(4,0);
        paneloeste.setLayout(gl3);
        
        //panel norte  
        JButton bn1 = new JButton("Agregar");
        JButton bn2 = new JButton("Modificar");
        JButton bn3 = new JButton("Borrar");
        
        //panel oeste
        String [] marca = {"Arcatel", "LG","Nokia","Samsumg"};
        String [] modelo = {"m1", "m2", "m3"};
            
        JLabel lb1 = new JLabel("No. de Compra:");
        JTextField tf1 = new JTextField(10);
        JLabel lb2 = new JLabel("Marca: ");
        JComboBox cb1 = new JComboBox(marca);
        JLabel lb3 = new JLabel("Modelo: ");
        JComboBox cb2 = new JComboBox(modelo);
        JLabel lb4 = new JLabel("Cantidad: ");
        JTextField tf2 = new JTextField(10);
        
        //panel sur
        JButton bs1 = new JButton("Guardar");
        JButton bs2 = new JButton("Cancelar");
                   
        //panel centro
        
        //se agregan cosas a los paneles
        panelnorte.add(bn1);
        panelnorte.add(bn2);
        panelnorte.add(bn3);
        
        gbc1.gridx = 0;
        gbc1.gridy = 0;
        //gbc1.weithy = 1.0;
        gbc1.anchor = GridBagConstraints.WEST;
        
        paneloeste.add(lb1);
        gbc1.gridy = 1;
        gbc1.gridwidth = 1;
        paneloeste.add(tf1);
        gbc1.gridy = 2;
        gbc1.gridwidth = 1;
        paneloeste.add(lb2);
        gbc1.gridy = 3;
        gbc1.gridwidth = 1;
        paneloeste.add(cb1);
        gbc1.gridy = 4;
        gbc1.gridwidth = 1;
        paneloeste.add(lb3);
        gbc1.gridy = 5;
        gbc1.gridwidth = 1;
        paneloeste.add(cb2);
        gbc1.gridy = 6;
        gbc1.gridwidth = 1;
        paneloeste.add(lb4);
        gbc1.gridy = 7;
        gbc1.gridwidth = 5;
        paneloeste.add(tf2);
        gbc1.gridy = 8;
        gbc1.gridwidth = 5;
        
        panelsur.add(bs1);
        panelsur.add(bs2);
                
        panelmayor.add(panelnorte, BorderLayout.NORTH);
        panelmayor.add(panelcentro, BorderLayout.CENTER);
        panelmayor.add(paneleste, BorderLayout.EAST);
        panelmayor.add(panelsur, BorderLayout.SOUTH);
        panelmayor.add(paneloeste, BorderLayout.WEST);
        
        f.add(panelmayor);
        
        f.setVisible(true);
    }

    void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
